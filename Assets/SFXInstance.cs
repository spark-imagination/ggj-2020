﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXInstance : MonoBehaviour
{
    private AudioSource src;
    private float timer = 0;
    private bool started = false;

    [SerializeField] private AudioClip click, connect, disconnect;

    public void PlayAudio(SFXSpawner.Sounds sound)
    {
        src = GetComponent<AudioSource>();

        switch(sound)
        {
            case SFXSpawner.Sounds.Click:
                src.PlayOneShot(click);
                timer = click.length * 2;
                break;

            case SFXSpawner.Sounds.Connect:
                src.PlayOneShot(connect);
                timer = connect.length * 2;
                break;

            case SFXSpawner.Sounds.Disconnect:
                src.PlayOneShot(disconnect);
                timer = disconnect.length * 2;
                break;
        }

        started = true;
    }

    void Update()
    {
        if(started)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
                Destroy(gameObject);
        }
    }
}
