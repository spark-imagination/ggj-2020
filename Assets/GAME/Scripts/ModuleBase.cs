﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ModuleBase : MonoBehaviour
{
    protected abstract bool TestCorrectness();
    public bool ModuleFinalized { get; private set; }
    [SerializeField] private ModuleStatusLight StatusLight;

    public void EjectComponents()
    {
        Connector[] connected = GetComponentsInChildren<Connector>();

        foreach (Connector conn in connected)
        {
            conn.Eject();
        }
    }

    void LateUpdate()
    {
        if(!ModuleFinalized)
        {
            ModuleFinalized = TestCorrectness();

            if(ModuleFinalized)
            {
                Connector[] connected = GetComponentsInChildren<Connector>();

                foreach(Connector conn in connected)
                {
                    if(conn.Connected != null)
                        conn.Connected.Locked = true;
                }

                StatusLight.SetStatus(true);
            }
        }
    }
}
