﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class X9_T802 : ModuleBase
{
    [SerializeField] private Connector switchConnector, buttonConnector, wireConnectorA, wireConnectorB;
    private bool buttonPressedCorrectly;

    protected override bool TestCorrectness()
    {
        return buttonPressedCorrectly;
    }

    void Start()
    {
        buttonConnector.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    bool ok = true;

                    if (switchConnector.Connected != null)
                    {
                        if (switchConnector.Connected is FlipSwitch)
                        {
                            ok = ((FlipSwitch)switchConnector.Connected).IsFlipped;
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    if (wireConnectorA.Connected != null)
                    {
                        if (wireConnectorA.Connected is WireHead)
                        {
                            if (((WireHead)wireConnectorA.Connected).MatchingWireHead != wireConnectorB.Connected)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    buttonPressedCorrectly = ok;

                    if (!ok)
                        EjectComponents();
                };
            }
        };

        buttonConnector.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };
    }
}
