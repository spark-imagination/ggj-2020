﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FCO_R1 : ModuleBase
{
    [SerializeField] private Connector light, flipswitch, goodBtn;

    private bool goodBtnPressed;

    protected override bool TestCorrectness()
    {
        return goodBtnPressed;
    }

    // Start is called before the first frame update
    void Start()
    {
        goodBtn.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    bool ok = true;

                    if (light.Connected != null)
                    {
                        if (light.Connected is LightComponent)
                        {
                            //ok
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    if (flipswitch.Connected != null)
                    {
                        if (flipswitch.Connected is FlipSwitch)
                        {
                            if (!((FlipSwitch)flipswitch.Connected).IsFlipped)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    goodBtnPressed = ok;

                    if (ok)
                        ((LightComponent)light.Connected).TurnOn(Color.blue);

                    if (!ok)
                        EjectComponents();
                };
            }
        };

        goodBtn.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };
    }
}
