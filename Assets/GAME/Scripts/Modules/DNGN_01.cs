﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNGN_01 : ModuleBase
{
    [SerializeField] private Connector wireA, wireB, light, badBtn, goodBtn;

    private bool goodBtnPressed;

    protected override bool TestCorrectness()
    {
        return goodBtnPressed;
    }

    void Start()
    {
        goodBtn.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    bool ok = true;

                    if (light.Connected != null)
                    {
                        if (light.Connected is LightComponent)
                        {
                            //ok
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    if (wireA.Connected != null)
                    {
                        if (wireA.Connected is WireHead)
                        {
                            if (((WireHead)wireA.Connected).MatchingWireHead != wireB.Connected)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    goodBtnPressed = ok;

                    if (ok)
                        ((LightComponent)light.Connected).TurnOn(Color.blue);

                    if (!ok)
                        EjectComponents();
                };
            }
        };

        goodBtn.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };

        //bad

        badBtn.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    EjectComponents();
                };
            }
            else
                EjectComponents();
        };

        badBtn.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
            else
                EjectComponents();
        };
    }
}
