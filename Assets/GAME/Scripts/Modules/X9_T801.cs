﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class X9_T801 : ModuleBase
{
    [SerializeField] private Connector lightConnector, buttonConnector, wireConnectorA, wireConnectorB;
    private bool buttonPressedCorrectly;

    protected override bool TestCorrectness()
    {
        return buttonPressedCorrectly;
    }

    void Start()
    {
        buttonConnector.OnConnect = c =>
        {
            if(c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () => 
                {
                    bool ok = true;

                    if(lightConnector.Connected != null)
                    {
                        if (lightConnector.Connected is LightComponent)
                        {
                            //ok
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    if (wireConnectorA.Connected != null)
                    {
                        if (wireConnectorA.Connected is WireHead)
                        {
                            if(((WireHead)wireConnectorA.Connected).MatchingWireHead != wireConnectorB.Connected)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    buttonPressedCorrectly = ok;

                    if (ok)
                        ((LightComponent)lightConnector.Connected).TurnOn(Color.green);

                    if (!ok)
                        EjectComponents();
                };
            }
        };

        buttonConnector.OnDisconnect = c => 
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };
    }
}
