﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SENSE_A : ModuleBase
{
    [SerializeField] private Connector bulbConnector, buttonConnector;

    private bool pressedCorrectly = false;
    private float offset;

    protected override bool TestCorrectness()
    {
        return pressedCorrectly;
    }

    // Start is called before the first frame update
    void Start()
    {
        offset = UnityEngine.Random.value * 10;
        
        bulbConnector.OnConnect = c =>
        {
            if (c is LightComponent)
            {
                ((LightComponent)c).TurnOn(Color.blue);
            }
            else
                EjectComponents();
        };
        
        buttonConnector.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    LightComponent bulb = null;

                    if (bulbConnector.Connected != null && bulbConnector.Connected is LightComponent)
                        bulb = (LightComponent)bulbConnector.Connected;

                    if (bulb != null && bulb.LightOn)
                    {
                        pressedCorrectly = true;
                        bulb.TurnOn(Color.green);
                    }
                    else
                        EjectComponents();
                };
            }
            else
                EjectComponents();
        };

        buttonConnector.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };
    }

    void Update()
    {
        if (!pressedCorrectly)
        {
            LightComponent bulb = null;

            if (bulbConnector.Connected != null && bulbConnector.Connected is LightComponent)
                bulb = (LightComponent)bulbConnector.Connected;

            if (bulb.LightOn)
            {
                if (Mathf.Sin((Time.realtimeSinceStartup + offset) * 3) <= 0)
                    bulb.TurnOff();
            }
            else
            {
                if (Mathf.Sin((Time.realtimeSinceStartup + offset) * 3) > 0)
                    bulb.TurnOn(Color.red);
            }
        }
    }
}
