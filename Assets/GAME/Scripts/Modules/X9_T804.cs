﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class X9_T804 : ModuleBase
{
    [SerializeField] private Connector lightConnector, switchConnector, wireConnectorA1, wireConnectorB1, wireConnectorA2, wireConnectorB2;
    private bool buttonPressedCorrectly;

    protected override bool TestCorrectness()
    {
        return buttonPressedCorrectly;
    }

    void Update()
    {
        if (switchConnector.Connected && switchConnector.Connected is FlipSwitch && ((FlipSwitch)switchConnector.Connected).IsFlipped)
        {
            bool ok = true;

            if (lightConnector.Connected != null)
            {
                if (lightConnector.Connected is LightComponent)
                {
                    //ok
                }
                else
                {
                    ok = false;
                }
            }
            else
            {
                ok = false;
            }
            
            if (wireConnectorA1.Connected != null)
            {
                if (wireConnectorA1.Connected is WireHead)
                {
                    if (((WireHead)wireConnectorA1.Connected).MatchingWireHead != wireConnectorA2.Connected)
                    {
                        ok = false;
                    }
                }
                else
                {
                    ok = false;
                }
            }
            else
            {
                ok = false;
            }

            if (wireConnectorB1.Connected != null)
            {
                if (wireConnectorB1.Connected is WireHead)
                {
                    if (((WireHead)wireConnectorB1.Connected).MatchingWireHead != wireConnectorB2.Connected)
                    {
                        ok = false;
                    }
                }
                else
                {
                    ok = false;
                }
            }
            else
            {
                ok = false;
            }

            buttonPressedCorrectly = ok;

            if (ok)
                ((LightComponent)lightConnector.Connected).TurnOn(Color.green);

            if (!ok)
                EjectComponents();
        }
    }
}
