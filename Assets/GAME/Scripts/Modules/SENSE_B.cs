﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SENSE_B : ModuleBase
{
    [SerializeField] private Connector wireA1, wireB1, wireA2, wireB2, light, goodBtn;

    private bool goodBtnPressed;

    protected override bool TestCorrectness()
    {
        return goodBtnPressed;
    }

    void Start()
    {
        goodBtn.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    bool ok = true;

                    if (light.Connected != null)
                    {
                        if (light.Connected is LightComponent)
                        {
                            //ok
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    if (wireA1.Connected != null)
                    {
                        if (wireA1.Connected is WireHead)
                        {
                            if (((WireHead)wireA1.Connected).MatchingWireHead != wireA2.Connected)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    if (wireB1.Connected != null)
                    {
                        if (wireB1.Connected is WireHead)
                        {
                            if (((WireHead)wireB1.Connected).MatchingWireHead != wireB2.Connected)
                            {
                                ok = false;
                            }
                        }
                        else
                        {
                            ok = false;
                        }
                    }
                    else
                    {
                        ok = false;
                    }

                    goodBtnPressed = ok;

                    if (ok)
                        ((LightComponent)light.Connected).TurnOn(Color.blue);

                    if (!ok)
                        EjectComponents();
                };
            }
        };

        goodBtn.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };
    }
}
