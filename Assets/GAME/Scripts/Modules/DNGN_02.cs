﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNGN_02 : ModuleBase
{
    [SerializeField] private Connector topWire, midWire, bottomWire, controlWire, topLight, midLight, bottomLight, controlButton;

    private bool topOn, midOn, bottomOn;

    private int colorindex = -1;

    private Color[] colors = new Color[]
    {
        Color.red,
        Color.green,
        Color.blue,
        Color.yellow
    };

    protected override bool TestCorrectness()
    {
        LightComponent tLight = null;
        LightComponent mLight = null;
        LightComponent bLight = null;

        if (topLight.Connected != null && topLight.Connected is LightComponent)
        {
            tLight = (LightComponent)topLight.Connected;
        }
        else return false;

        if (midLight.Connected != null && midLight.Connected is LightComponent)
        {
            mLight = (LightComponent)midLight.Connected;
        }
        else return false;

        if (bottomLight.Connected != null && bottomLight.Connected is LightComponent)
        {
            bLight = (LightComponent)bottomLight.Connected;
        }
        else return false;

        switch (colorindex)
        {
            case 0: //red
                if (!tLight.LightOn && mLight.LightOn && !bLight.LightOn)
                    return true;
                break;

            case 1: //green
                if (tLight.LightOn && !mLight.LightOn && bLight.LightOn)
                    return true;
                break;

            case 2: //blue
                if (tLight.LightOn && !mLight.LightOn && !bLight.LightOn)
                    return true;
                break;

            case 3: //yellow
                if (tLight.LightOn && mLight.LightOn && bLight.LightOn)
                    return true;
                break;
        }

        return false;
    }

    // Start is called before the first frame update
    void Start()
    {
        colorindex = UnityEngine.Random.Range(0, colors.Length);

        topLight.OnConnect = c =>
        {
            if (c is LightComponent)
            {
                LightComponent light = (LightComponent)c;

                if (topOn)
                    light.TurnOn(colors[colorindex]);
                else
                    light.TurnOff();
            }
            else
                EjectComponents();
        };

        midLight.OnConnect = c =>
        {
            if (c is LightComponent)
            {
                LightComponent light = (LightComponent)c;

                if (midOn)
                    light.TurnOn(colors[colorindex]);
                else
                    light.TurnOff();
            }
            else
                EjectComponents();
        };

        bottomLight.OnConnect = c =>
        {
            if (c is LightComponent)
            {
                LightComponent light = (LightComponent)c;

                if (bottomOn)
                    light.TurnOn(colors[colorindex]);
                else
                    light.TurnOff();
            }
            else
                EjectComponents();
        };

        controlButton.OnConnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = () =>
                {
                    if (controlWire.Connected != null && controlWire.Connected is WireHead)
                    {
                        WireHead wire = (WireHead)controlWire.Connected;

                        if(wire.MatchingWireHead.ConnectedConnector != null)
                        {
                            Connector connected = wire.MatchingWireHead.ConnectedConnector;

                            bool correct = false;

                            if(connected == topWire)
                            {
                                correct = true;
                                ToggleTop();
                            }

                            if(connected == midWire)
                            {
                                correct = true;
                                ToggleMid();
                            }

                            if(connected == bottomWire)
                            {
                                correct = true;
                                ToggleBottom();
                            }

                            if (!correct)
                                EjectComponents();
                        }
                        else
                            EjectComponents();
                    }
                    else
                        EjectComponents();
                };
            }
        };

        controlButton.OnDisconnect = c =>
        {
            if (c is Pushbutton)
            {
                Pushbutton btn = (Pushbutton)c;

                btn.OnClick = null;
            }
        };
    }

    private void ToggleTop()
    {
        topOn = !topOn;

        if (topLight.Connected && topLight.Connected is LightComponent)
        {
            if (topOn)
                ((LightComponent)topLight.Connected).TurnOn(colors[colorindex]);
            else
                ((LightComponent)topLight.Connected).TurnOff();
        }
    }

    private void ToggleMid()
    {
        midOn = !midOn;

        if (midLight.Connected && midLight.Connected is LightComponent)
        {
            if (midOn)
                ((LightComponent)midLight.Connected).TurnOn(colors[colorindex]);
            else
                ((LightComponent)midLight.Connected).TurnOff();
        }
    }

    private void ToggleBottom()
    {
        bottomOn = !bottomOn;

        if (bottomLight.Connected && bottomLight.Connected is LightComponent)
        {
            if (bottomOn)
                ((LightComponent)bottomLight.Connected).TurnOn(colors[colorindex]);
            else
                ((LightComponent)bottomLight.Connected).TurnOff();
        }
    }
}
