﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connector : MonoBehaviour
{
    public Connectable Connected { get; private set; }
    public Transform ConnectionPoint;

    public ConnectorTypes ConnectorType;

    public OnConnectDelegate OnConnect;
    public OnDisconnectDelegate OnDisconnect;

    public delegate void OnConnectDelegate(Connectable connected);
    public delegate void OnDisconnectDelegate(Connectable disconnected);

    public void Connect(Connectable conn)
    {
        Connected = conn;
        OnConnect?.Invoke(Connected);
    }

    public void Disconnect(Connectable conn)
    {
        if (conn != Connected)
            throw new InvalidOperationException("Wrong object disconnected");

        OnDisconnect?.Invoke(Connected);

        Connected = null;
    }

    public void Eject()
    {
        if (Connected != null)
        {
            Connected.Eject();
            Connected = null;
        }
    }

    public enum ConnectorTypes
    {
        Round, Square
    }
}
