﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyLight : MonoBehaviour
{
    private Tween blinkTween = new Tween(Easing.EaseInOutCubic);
    [SerializeField] private Renderer lightModelRenderer;
    [SerializeField] private Light light;
    private bool on = false;

    // Start is called before the first frame update
    void Start()
    {
        blinkTween.onChange = t => 
        {
            light.range = t * 10;
            lightModelRenderer.material.color = Color.Lerp(Color.black, Color.red, t);
            light.intensity = t * 10;
        };
    }

    // Update is called once per frame
    void Update()
    {
        if(!blinkTween.active)
        {
            if(on)
            {
                on = false;
                blinkTween.Start(1, 0, 0.8f);
            }
            else
            {
                on = true;
                blinkTween.Start(0, 1, 0.8f);
            }
        }
    }
}
