﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleStatusLight : MonoBehaviour
{
    [SerializeField] private Material statusGood, statusBad;
    [SerializeField] private Renderer statusRenderer;

    public void SetStatus(bool good)
    {
        statusRenderer.material = good ? statusGood : statusBad;
    }
}
