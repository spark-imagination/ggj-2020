﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pushbutton : Connectable
{
    public PushButtonOnClick OnClick;
    [SerializeField] private float buttonRestPosition, buttonClickedPosition;
    [SerializeField] private Transform buttonTransform;
    private Tween pushTween = new Tween(Easing.Linear);

    void Start()
    {
        pushTween.onChange = t =>
        {
            buttonTransform.localPosition = Curving.Lerp(new Vector3[]
                {
                new Vector3(0, buttonRestPosition, 0), 
                new Vector3(0, buttonClickedPosition, 0),
                new Vector3(0, buttonRestPosition, 0)
                }, t);
        };
    }

    public override void Activate()
    {
        base.Activate();

        SFXSpawner.StartSFX(SFXSpawner.Sounds.Click, transform.position);

        pushTween.Start(0, 1, 0.6f);

        OnClick?.Invoke();
    }

    public delegate void PushButtonOnClick();
}
