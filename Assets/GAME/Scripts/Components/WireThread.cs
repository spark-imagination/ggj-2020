﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireThread : MonoBehaviour
{
    [SerializeField] private Transform wireThreadStartA, wireThreadStartB;
    private LineRenderer lineRenderer;

    private float lineBendSize = 0.3f;
    private int lineSections = 50;

    private Color[] colors =
    {
        Color.green,
        Color.blue,
        Color.red,
        Color.yellow,
        Color.black,
        Color.white,
        Color.cyan,
        Color.gray
    };

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        Color color = colors[Random.Range(0, colors.Length)];
        lineRenderer.SetColors(color, color);

        lineRenderer.positionCount = lineSections;
    }

    void FixedUpdate()
    {
        Vector3[] points = new Vector3[]
        {
            wireThreadStartA.transform.position,
            wireThreadStartA.transform.position + (wireThreadStartA.transform.forward * lineBendSize),
            wireThreadStartB.transform.position + (wireThreadStartB.transform.forward * lineBendSize),
            wireThreadStartB.transform.position
        };

        for(int i = 0; i < lineSections; i++)
        {
            lineRenderer.SetPosition(i, Curving.Lerp(points, (float)i / (float)(lineSections - 1)));
        }
    }
}
