﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipSwitch : Connectable
{
    [SerializeField] private Transform flipper;
    [SerializeField] private float offRotation, onRotation;
    public bool IsFlipped { get; private set; }

    public override void Activate()
    {
        base.Activate();

        SFXSpawner.StartSFX(SFXSpawner.Sounds.Click, transform.position);

        IsFlipped = !IsFlipped;

        flipper.transform.localRotation = Quaternion.Euler(IsFlipped ? onRotation : offRotation, 90, 90);
    }
}
