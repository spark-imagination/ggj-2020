﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightComponent : Connectable
{
    [SerializeField] private Material offMat, onMat;
    [SerializeField] private Renderer lightRenderer;
    public bool LightOn { get; private set; }

    public override void OnConnect()
    {
        base.OnConnect();
    }

    public override void OnDisconnect()
    {
        base.OnDisconnect();
        TurnOff();
    }

    public void TurnOff()
    {
        lightRenderer.material = offMat;
        LightOn = false;
    }

    public void TurnOn(Color color)
    {
        lightRenderer.material = onMat;
        lightRenderer.material.color = color;

        LightOn = true;
    }
}
