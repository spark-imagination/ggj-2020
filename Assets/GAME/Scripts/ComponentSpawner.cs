﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentSpawner : MonoBehaviour
{
    [SerializeField] private GameObject componentToSpawn;
    private float spawnDistance = 0.2f;
    private GameObject spawned;

    void Update()
    {
        if (spawned != null)
        {
            bool hasStill = false;
            foreach (Grabbable coll in spawned.GetComponentsInChildren<Grabbable>())
            {
                if (Vector3.Distance(transform.position, coll.transform.position) < spawnDistance)
                    hasStill = true;
            }

            if(!hasStill)
            {
                /*foreach (Collider coll in spawned.GetComponentsInChildren<Collider>())
                {
                    coll.enabled = true;
                }*/

                spawned = null;
            }
        }
        else
        {
            spawned = Instantiate(componentToSpawn, transform.position, transform.rotation);
            Grabbable[] grabbables = spawned.GetComponentsInChildren<Grabbable>();

            foreach(Grabbable grabbable in grabbables)
            {
                grabbable.FreezeRigidbody();
            }

            /*foreach (Collider coll in spawned.GetComponentsInChildren<Collider>())
            {
                coll.enabled = false;
            }*/
        }
    }
}
