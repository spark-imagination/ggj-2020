﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModuleSpawnerController : MonoBehaviour
{
    private ModuleSlot[] slots;
    private float timePerModule = 60;
    public float SecondsRemainingUntilLoss = -1;

    void Start()
    {
        slots = GetComponentsInChildren<ModuleSlot>();

        int moduleSpawns = Random.Range(1, slots.Length);

        SecondsRemainingUntilLoss = timePerModule * moduleSpawns;

        slots = slots.OrderBy(o => Random.value).ToArray();

        for(int i = 0; i < moduleSpawns; i++)
        {
            slots[i].Spawn();
        }
    }

    void Update()
    {
        bool win = true;
        SecondsRemainingUntilLoss -= Time.deltaTime;

        foreach(ModuleSlot slot in slots)
        {
            if(slot.Module != null)
            {
                if (!slot.Module.ModuleFinalized)
                    win = false;
            }
        }

        if(win)
        {
            SceneManager.LoadScene(1);
        }

        if(SecondsRemainingUntilLoss <= 0)
        {
            SceneManager.LoadScene(2);
        }
    }
}
