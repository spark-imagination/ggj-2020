﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connectable : Grabbable
{
    [SerializeField] private float connectRange = 0.15f;
    [SerializeField] private Vector3 connectionOffset;
    [SerializeField] private Vector3 rotationOffset;
    [SerializeField] private Connector.ConnectorTypes ConnectorType;

    public bool ConnectTest;

    public Connector ConnectedConnector;

    void Update()
    {
        if(ConnectTest)
        {
            ConnectTest = false;
            TryConnect();
        }
    }

    public virtual void Activate() { }

    public virtual void OnConnect()
    {
        SFXSpawner.StartSFX(SFXSpawner.Sounds.Connect, transform.position);
    }

    public virtual void OnDisconnect()
    {
        SFXSpawner.StartSFX(SFXSpawner.Sounds.Disconnect, transform.position);
    }

    public override void Grab(ObjectGrabber grabber)
    {
        base.Grab(grabber);

        if(ConnectedConnector != null)
        {
            ConnectedConnector.Disconnect(this);
            OnDisconnect();
        }
    }

    public override void Release(Vector3 velocity, Vector3 torque)
    {
        base.Release(velocity, torque);

        TryConnect();
    }

    public void Eject()
    {
        if(ConnectedConnector != null)
        {
            ConnectedConnector = null;
            UnfreezeRigidbody();

            rigidbody.AddTorque(Random.insideUnitSphere * 10);
            rigidbody.AddForce(Random.insideUnitSphere * 100);

            OnDisconnect();
        }
    }

    private void TryConnect()
    {
        Connector[] connectors = FindObjectsOfType<Connector>();
        Connector found = null;
        float dist = connectRange;

        for (int i = 0; i < connectors.Length; i++)
        {
            float aDist = Vector3.Distance(transform.position, connectors[i].transform.position);

            if (aDist < dist && connectors[i].Connected == null && connectors[i].ConnectorType == ConnectorType)
            {
                dist = aDist;
                found = connectors[i];
            }
        }

        if (found != null)
        {
            ConnectedConnector = found;
            rigidbody.isKinematic = true;
            found.Connect(this);

            rigidbody.constraints = RigidbodyConstraints.FreezeAll;

            Vector3 offset = (found.ConnectionPoint.right * connectionOffset.x) + (found.ConnectionPoint.up * connectionOffset.y) + (found.ConnectionPoint.forward * connectionOffset.z);

            transform.position = found.ConnectionPoint.position + offset;
            transform.rotation = found.ConnectionPoint.rotation;

            transform.Rotate(rotationOffset);

            OnConnect();
        }
    }
}
