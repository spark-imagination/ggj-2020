﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToGame : MonoBehaviour
{
    private float timer = 5;
    private bool done = false;

    void Update()
    {
        if(!done)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
                SceneManager.LoadScene(0);
        }
    }
}
