﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleSlot : MonoBehaviour
{
    [SerializeField] private ModuleBase[] modulePrefabs;
    public ModuleBase Module { get; private set; }

    void Awake()
    {
        transform.LookAt(new Vector3(0, transform.position.y, 0));
    }

    public void Spawn()
    {
        Module = Instantiate(modulePrefabs[Random.Range(0, modulePrefabs.Length)], transform.position, transform.rotation);
    }
}
