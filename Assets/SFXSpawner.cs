﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXSpawner : MonoBehaviour
{
    private static SFXSpawner instance;
    [SerializeField] private SFXInstance sfxPrefab;

    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }

    public static void StartSFX(Sounds sound, Vector3 position)
    {
        SFXInstance sfx = Instantiate(instance.sfxPrefab, position, Quaternion.identity);
        sfx.PlayAudio(sound);
    }

    public enum Sounds
    {
        Connect, Disconnect, Click
    }
}
