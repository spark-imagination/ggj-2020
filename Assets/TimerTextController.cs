﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerTextController : MonoBehaviour
{
    private TextMesh textMesh;
    [SerializeField] private ModuleSpawnerController timecontroller;

    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        int seconds = Mathf.RoundToInt(timecontroller.SecondsRemainingUntilLoss);

        int minutes = 0;

        while(seconds >= 60)
        {
            seconds -= 60;
            minutes++;
        }

        string secondString = seconds.ToString();

        if (secondString.Length == 1)
            secondString = "0" + secondString;

        string minuteString = minutes.ToString();

        if (minuteString.Length == 1)
            minuteString = "0" + minuteString;

        textMesh.text = minuteString + ":" + secondString;
    }
}
