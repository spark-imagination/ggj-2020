﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Grabbable : MonoBehaviour
{
    private ObjectGrabber grabbedByObject;
    [HideInInspector] public Rigidbody rigidbody;
    private Transform oldParent;
    public bool Locked { get; set; }

    public bool IsGrabbed
    {
        get
        {
            return grabbedByObject != null;
        }
    }

    // Start is called before the first frame update
    void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        oldParent = transform.parent;
    }

    public void FreezeRigidbody()
    {
        rigidbody.isKinematic = true;
        rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }

    public void UnfreezeRigidbody()
    {
        rigidbody.isKinematic = false;
        rigidbody.constraints = RigidbodyConstraints.None;
    }

    public virtual void Grab(ObjectGrabber grabber)
    {
        FreezeRigidbody();

        transform.parent = grabber.transform;

        grabbedByObject = grabber;
    }

    public virtual void Release(Vector3 velocity, Vector3 torque)
    {
        UnfreezeRigidbody();
        rigidbody.velocity = (velocity);
        rigidbody.AddTorque(torque);

        transform.parent = oldParent;
        grabbedByObject = null;
    }
}
