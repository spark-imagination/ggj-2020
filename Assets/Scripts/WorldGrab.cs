﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGrab : MonoBehaviour
{
    [SerializeField] private Controllers type;
    [SerializeField] private Transform handTransform;
    [SerializeField] private Transform baseTransform;
    public enum Controllers
    {
        Primary_Left, Secondary_Right
    }

    private bool grab = false;
    private Vector3 handStart = Vector3.zero;
    private Vector3 headStart = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        if(grab == false)
        {
            handStart = handTransform.localPosition;
            headStart = baseTransform.position;
        }

        if (type == Controllers.Primary_Left)
        {
            grab = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0.5f;
        }

        if (type == Controllers.Secondary_Right)
        {
            grab = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.5f;
        }

        if (grab)
        {
            Vector3 delta = handTransform.localPosition - handStart;

            baseTransform.position = headStart - delta;
        }
    }
}
