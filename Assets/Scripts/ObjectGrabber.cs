﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGrabber : MonoBehaviour
{
    [SerializeField] private Controllers type;
    private Grabbable grabbed;
    private Transform oldParent;

    private float grabDistance = 0.2f;

    private Vector3 oldPosition = Vector3.zero;
    private Vector3 oldRotation;

    private float activateTimer = 0;

    public enum Controllers
    {
        Primary_Left, Secondary_Right
    }

    private bool grab = false;

    // Update is called once per frame
    void Update()
    {
        activateTimer -= Time.deltaTime;
        bool flip = false;

        if (activateTimer <= 0)
        {
            if (type == Controllers.Primary_Left)
            {
                flip = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger) > 0.5f;
            }

            if (type == Controllers.Secondary_Right)
            {
                flip = OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) > 0.5f;
            }

            if (flip && grabbed == null)
            {
                Connectable[] bodies = FindObjectsOfType<Connectable>();

                Connectable body = null;
                float d = grabDistance * 1.6f;

                for (int i = 0; i < bodies.Length; i++)
                {
                    float dist = Vector3.Distance(transform.position, bodies[i].transform.position);

                    if (dist < d && !bodies[i].Locked)
                    {
                        body = bodies[i];
                        d = dist;
                    }
                }

                body.Activate();
                activateTimer = 0.6f;
            }
        }
        // ^ACTIVATE   vGRAB
        if (grab == false)
        {
            if(grabbed != null)
            {
                grabbed.Release((transform.position - oldPosition) * 100, (transform.rotation.eulerAngles - oldRotation) * 2);
                grabbed = null;
            }
        }
        
        if (type == Controllers.Primary_Left)
        {
            grab = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger) > 0.5f;
        }

        if (type == Controllers.Secondary_Right)
        {
            grab = OVRInput.Get(OVRInput.Axis1D.SecondaryHandTrigger) > 0.5f;
        }

        if (grab && grabbed == null)
        {
            Grabbable[] bodies = FindObjectsOfType<Grabbable>();

            Grabbable body = null;
            float d = grabDistance;

            for(int i = 0; i < bodies.Length; i++)
            {
                float dist = Vector3.Distance(transform.position, bodies[i].transform.position);

                if(dist < d && !bodies[i].IsGrabbed && !bodies[i].Locked)
                {
                    body = bodies[i];
                    d = dist;
                }
            }

            grabbed = body;

            if(grabbed != null)
            {
                grabbed.Grab(this);
            }
        }

        oldPosition = transform.position;
        oldRotation = transform.rotation.eulerAngles;
    }
}
