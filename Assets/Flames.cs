﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flames : MonoBehaviour
{
    [SerializeField] private Renderer renderer;

    void Update()
    {
        renderer.sharedMaterial.mainTextureOffset = new Vector2(0, Random.value * 10);
    }
}
